all:
	$(MAKE) -C iopcore/cdvdfsv all
	$(MAKE) -C iopcore/cdvdman USE_BDM=1 IOPCORE_DEBUG=0 SPEED_TESTING=1 all
	$(MAKE) -C iop all
	$(MAKE) -C ee all

format:
	find . -type f -not -path \*modules/network/SMSTCPIP\* -a \( -iname \*.h -o -iname \*.c \) | xargs clang-format -i

clean:
	$(MAKE) -C iopcore/cdvdfsv clean
	$(MAKE) -C iopcore/cdvdman USE_BDM=1 clean
	$(MAKE) -C iop clean
	$(MAKE) -C ee clean

run: all
	ps2client -h 192.168.1.10 execee host:ee/rw_speed.elf
