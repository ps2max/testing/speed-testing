#ifndef CONFIG_H
#define CONFIG_H

// Blocks sizes to test
#define READ_SIZE (1 * 1024 * 1024)
#define BLOCK_SIZE_MIN (512)
#define BLOCK_SIZE_MAX (256 * 1024)
// Files to use
//#define MASS_FILE_NAME "DVD/SLES_549.45.DragonBall Z Budokai Tenkaichi 3.iso"
#define MASS_FILE_NAME "DVD/SCUS_973.28.Gran Turismo 4.iso"
#define MASS0_FILE_NAME "mass0:" MASS_FILE_NAME
#define MASS1_FILE_NAME "mass1:" MASS_FILE_NAME
#define SMB_FILE_NAME "smb0:" MASS_FILE_NAME
#define CDVD_FILE_NAME "cdrom:DATA/PZS3EU1.AFS"
#define HOST_FILE_NAME "host:zero.bin"
#define PFS_FILE_NAME "pfs0:zero.bin" // Place 'zero.bin' inside __system partition of internal HDD (use uLE)

// Load one of these Block Device Managers
#define LOAD_BDM
//#define LOAD_BDM_CDVD

// Load one of these Block Devices
#define LOAD_BD_USB
//#define LOAD_BD_MX4SIO
//#define LOAD_BD_IEEE

// Load one or more File Systems
//#define LOAD_FS_VFAT

// Other file systems
//#define LOAD_PFS
//#define LOAD_HOST
#define LOAD_SMB

// Where to run the tests
//#define TEST_ON_EE
#define TEST_ON_IOP

#endif

/*
// usbmass_bd (ASYNC) on FAT-IOP (old IOP toolchain)
Start reading 'usb0p0' block device:
Read 1024KiB in 4099ms, blocksize=512, speed=255KB/s
Read 1024KiB in 3063ms, blocksize=1024, speed=342KB/s
Read 1024KiB in 2048ms, blocksize=2048, speed=512KB/s
Read 1024KiB in 1537ms, blocksize=4096, speed=682KB/s
Read 1024KiB in 1280ms, blocksize=8192, speed=819KB/s
Read 1024KiB in 1217ms, blocksize=16384, speed=861KB/s
Read 1024KiB in 1152ms, blocksize=32768, speed=910KB/s
Read 1024KiB in 1106ms, blocksize=65536, speed=948KB/s
Read 1024KiB in 1105ms, blocksize=131072, speed=948KB/s
Read 1024KiB in 1105ms, blocksize=262144, speed=948KB/s

// usbmass_bd (ASYNC) via cdvdman on FAT-IOP (old IOP toolchain)
Start reading file cdrom:DATA/PZS3EU1.AFS:
Read 1024KiB in 8196ms, blocksize=512, speed=127KB/s
Read 1024KiB in 4099ms, blocksize=1024, speed=255KB/s
Read 1024KiB in 2048ms, blocksize=2048, speed=512KB/s
Read 1024KiB in 1536ms, blocksize=4096, speed=682KB/s
Read 1024KiB in 1281ms, blocksize=8192, speed=818KB/s
Read 1024KiB in 1216ms, blocksize=16384, speed=862KB/s
Read 1024KiB in 1152ms, blocksize=32768, speed=910KB/s
Read 1024KiB in 1110ms, blocksize=65536, speed=944KB/s
Read 1024KiB in 1108ms, blocksize=131072, speed=946KB/s
Read 1024KiB in 1108ms, blocksize=262144, speed=946KB/s

// usbmass_bd (ASYNC) on FAT-IOP (new IOP toolchain)
Start reading 'usb0p0' block device:
Read 1024KiB in 4099ms, blocksize=512, speed=255KB/s
Read 1024KiB in 2054ms, blocksize=1024, speed=510KB/s
Read 1024KiB in 1543ms, blocksize=2048, speed=679KB/s
Read 1024KiB in 1289ms, blocksize=4096, speed=813KB/s
Read 1024KiB in 1284ms, blocksize=8192, speed=816KB/s
Read 1024KiB in 1158ms, blocksize=16384, speed=905KB/s
Read 1024KiB in 1126ms, blocksize=32768, speed=931KB/s
Read 1024KiB in 1110ms, blocksize=65536, speed=944KB/s
Read 1024KiB in 1110ms, blocksize=131072, speed=944KB/s
Read 1024KiB in 1110ms, blocksize=262144, speed=944KB/s

// mx4sio_bd on FAT-IOP (new IOP toolchain, no crc)
Start reading 'sdc0p0' block device:
Read 1024KiB in 1865ms, blocksize=512, speed=562KB/s
Read 1024KiB in 1235ms, blocksize=1024, speed=849KB/s
Read 1024KiB in 933ms, blocksize=2048, speed=1123KB/s
Read 1024KiB in 780ms, blocksize=4096, speed=1344KB/s
Read 1024KiB in 706ms, blocksize=8192, speed=1485KB/s
Read 1024KiB in 661ms, blocksize=16384, speed=1586KB/s
Read 1024KiB in 640ms, blocksize=32768, speed=1638KB/s
Read 1024KiB in 633ms, blocksize=65536, speed=1656KB/s
Read 1024KiB in 629ms, blocksize=131072, speed=1667KB/s
Read 1024KiB in 626ms, blocksize=262144, speed=1675KB/s

// mx4sio_bd on FAT-IOP via cdvdman (old IOP toolchain, no crc)
Start reading file cdrom:DATA/PZS3EU1.AFS:
Read 1024KiB in 4006ms, blocksize=512, speed=261KB/s
Read 1024KiB in 2030ms, blocksize=1024, speed=516KB/s
Read 1024KiB in 990ms, blocksize=2048, speed=1059KB/s
Read 1024KiB in 803ms, blocksize=4096, speed=1305KB/s
Read 1024KiB in 718ms, blocksize=8192, speed=1460KB/s
Read 1024KiB in 676ms, blocksize=16384, speed=1551KB/s
Read 1024KiB in 646ms, blocksize=32768, speed=1623KB/s
Read 1024KiB in 632ms, blocksize=65536, speed=1659KB/s
Read 1024KiB in 625ms, blocksize=131072, speed=1677KB/s
Read 1024KiB in 620ms, blocksize=262144, speed=1691KB/s

// SMB on slim-IOP (old IOP toolchain)
Start reading file smb0:DVD/SLES_549.45.DragonBall Z Budokai Tenkaichi 3.iso:
Read 1024KiB in 2450ms, blocksize=512, speed=427KB/s
Read 1024KiB in 1392ms, blocksize=1024, speed=753KB/s
Read 1024KiB in 1106ms, blocksize=2048, speed=948KB/s
Read 1024KiB in 777ms, blocksize=4096, speed=1349KB/s
Read 1024KiB in 696ms, blocksize=8192, speed=1506KB/s
Read 1024KiB in 649ms, blocksize=16384, speed=1615KB/s
Read 1024KiB in 7353ms, blocksize=32768, speed=142KB/s
Read 1024KiB in 895ms, blocksize=65536, speed=1171KB/s
Read 1024KiB in 527ms, blocksize=131072, speed=1989KB/s
Read 1024KiB in 524ms, blocksize=262144, speed=2001KB/s

// SMB on FAT-IOP (new IOP toolchain)
Start reading file smb0:DVD/SCUS_973.28.Gran Turismo 4.iso:
Read 1024KiB in 3172ms, blocksize=512, speed=330KB/s
Read 1024KiB in 1722ms, blocksize=1024, speed=608KB/s
Read 1024KiB in 1374ms, blocksize=2048, speed=763KB/s
Read 1024KiB in 966ms, blocksize=4096, speed=1085KB/s
Read 1024KiB in 868ms, blocksize=8192, speed=1208KB/s
Read 1024KiB in 812ms, blocksize=16384, speed=1291KB/s
Read 1024KiB in 775ms, blocksize=32768, speed=1353KB/s
Read 1024KiB in 784ms, blocksize=65536, speed=1337KB/s
Read 1024KiB in 757ms, blocksize=131072, speed=1385KB/s
Read 1024KiB in 760ms, blocksize=262144, speed=1379KB/s

// SMB on slim-IOP (new IOP toolchain)
Start reading file smb0:DVD/SCUS_973.28.Gran Turismo 4.iso:
Read 1024KiB in 2364ms, blocksize=512, speed=443KB/s
Read 1024KiB in 1311ms, blocksize=1024, speed=799KB/s
Read 1024KiB in 1040ms, blocksize=2048, speed=1008KB/s
Read 1024KiB in 743ms, blocksize=4096, speed=1411KB/s
Read 1024KiB in 671ms, blocksize=8192, speed=1562KB/s
Read 1024KiB in 625ms, blocksize=16384, speed=1677KB/s
Read 1024KiB in 602ms, blocksize=32768, speed=1741KB/s
Read 1024KiB in 604ms, blocksize=65536, speed=1736KB/s
Read 1024KiB in 585ms, blocksize=131072, speed=1792KB/s
Read 1024KiB in 588ms, blocksize=262144, speed=1783KB/s

// UDPBD on slim-IOP (old IOP toolchain)
Start reading 'udp0p0' block device:
Read 1024KiB in 544ms, blocksize=512, speed=1927KB/s
Read 1024KiB in 329ms, blocksize=1024, speed=3187KB/s
Read 1024KiB in 211ms, blocksize=2048, speed=4969KB/s
Read 1024KiB in 155ms, blocksize=4096, speed=6765KB/s
Read 1024KiB in 116ms, blocksize=8192, speed=9039KB/s
Read 1024KiB in 104ms, blocksize=16384, speed=10082KB/s
Read 1024KiB in 97ms, blocksize=32768, speed=10810KB/s
Read 1024KiB in 92ms, blocksize=65536, speed=11397KB/s
Read 1024KiB in 92ms, blocksize=131072, speed=11397KB/s
Read 1024KiB in 92ms, blocksize=262144, speed=11397KB/s

// PATA HDD on FAT-IOP (old IOP toolchain)
Start reading file pfs0:zero.bin:
Read 10240KiB in 5991ms, blocksize=512, speed=1750KB/s
Read 10240KiB in 3054ms, blocksize=1024, speed=3433KB/s
Read 10240KiB in 1587ms, blocksize=2048, speed=6607KB/s
Read 10240KiB in 844ms, blocksize=4096, speed=12423KB/s
Read 10240KiB in 504ms, blocksize=8192, speed=20805KB/s
Read 10240KiB in 327ms, blocksize=16384, speed=32066KB/s
Read 10240KiB in 323ms, blocksize=32768, speed=32463KB/s
Read 10240KiB in 325ms, blocksize=65536, speed=32263KB/s
Read 10240KiB in 340ms, blocksize=131072, speed=30840KB/s
Read 10240KiB in 324ms, blocksize=262144, speed=32363KB/s

// IEEE1394 on FAT-IOP (old IOP toolchain)
Start reading 'sd0p0' block device:
Read 1024KiB in 958ms, blocksize=512, speed=1094KB/s
Read 1024KiB in 537ms, blocksize=1024, speed=1952KB/s
Read 1024KiB in 330ms, blocksize=2048, speed=3177KB/s
Read 1024KiB in 224ms, blocksize=4096, speed=4681KB/s
Read 1024KiB in 186ms, blocksize=8192, speed=5637KB/s
Read 1024KiB in 165ms, blocksize=16384, speed=6355KB/s
Read 1024KiB in 162ms, blocksize=32768, speed=6472KB/s
Read 1024KiB in 163ms, blocksize=65536, speed=6432KB/s
Read 1024KiB in 159ms, blocksize=131072, speed=6594KB/s
Read 1024KiB in 157ms, blocksize=262144, speed=6678KB/s

// host: on FAT-IOP (old IOP toolchain)
Start reading file host:zero.bin:
Read 10240KiB in 11753ms, blocksize=2048, speed=892KB/s
Read 10240KiB in 8516ms, blocksize=4096, speed=1231KB/s
Read 10240KiB in 6674ms, blocksize=8192, speed=1571KB/s
Read 10240KiB in 6060ms, blocksize=16384, speed=1730KB/s
Read 10240KiB in 5703ms, blocksize=32768, speed=1838KB/s
Read 10240KiB in 5515ms, blocksize=65536, speed=1901KB/s
Read 10240KiB in 5422ms, blocksize=131072, speed=1933KB/s
Read 10240KiB in 5395ms, blocksize=262144, speed=1943KB/s

*/
